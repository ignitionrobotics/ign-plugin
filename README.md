# REPOSITORY MOVED

## This repository has moved to

https://github.com/ignitionrobotics/ign-plugin

## Issues and pull requests are backed up at

https://osrf-migration.github.io/ignition-gh-pages/#!/ignitionrobotics/ign-plugin

## Until BitBucket removes Mercurial support, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/ign-plugin

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

